var api_key = 'key-48454d72c4164112ee79fa8bce7a62e6';
var domain = 'mg.fishersupplyla.net';
var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});
var express = require('express');
var multiparty = require('connect-multiparty');
var path = require('path');
const fs = require('fs-extra');
var multipartyMiddleware = multiparty();
var emailRouter = express.Router();

emailRouter.use(function(req, res, next){
    return next();
});

var plantillaHtml = '';
var sendNotification = function(email, name){
    plantillaHtml =  '   <html>     '  +
        '      <head>  '  +
        '         <meta charset=UTF-8>  '  +
        '         <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/angular_material/1.0.0/angular-material.min.css">  '  +
        '         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">  '  +
        '         <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">  '  +
        '         <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">  '  +
        '         <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>  '  +
        '         <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-animate.min.js"></script>  '  +
        '         <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-aria.min.js"></script>  '  +
        '         <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-messages.min.js"></script>  '  +
        '         <script src="http://ajax.googleapis.com/ajax/libs/angular_material/1.0.0/angular-material.min.js"></script>           '  +
        '         <style>  '  +
        '            .footer-distributed{  '  +
        '               background-color: #24282B;  '  +
        '               box-shadow: 0 1px 1px 0 rgba(0, 0, 0, 0.12);  '  +
        '               box-sizing: border-box;  '  +
        '               width: 100%;  '  +
        '               text-align: left;  '  +
        '               font: bold 16px sans-serif;  '  +
        '     '  +
        '               padding: 55px 50px;  '  +
        '               margin-top: 80px;  '  +
        '            }  '  +
        '     '  +
        '            .footer-distributed .footer-left,  '  +
        '            .footer-distributed .footer-center,  '  +
        '            .footer-distributed .footer-right{  '  +
        '               display: inline-block;  '  +
        '               vertical-align: top;  '  +
        '            }  '  +
        '     '  +
        '            /* Footer left */  '  +
        '     '  +
        '            .footer-distributed .footer-left{  '  +
        '               width: 40%;  '  +
        '            }  '  +
        '     '  +
        '            /* The company logo */  '  +
        '     '  +
        '            .footer-distributed h3{  '  +
        '               color:  #ffffff;  '  +
        '               font: normal 36px, bold;  '  +
        '               margin: 0;  '  +
        '            }  '  +
        '     '  +
        '            .footer-distributed h3 span{  '  +
        '               color:  #5383d3;  '  +
        '            }  '  +
        '     '  +
        '            /* Footer links */  '  +
        '     '  +
        '            .footer-distributed .footer-links{  '  +
        '               color:  #ffffff;  '  +
        '               padding: 0;  '  +
        '               margin-left: 10px;  '  +
        '               margin-bottom: 5px;  '  +
        '            }  '  +
        '     '  +
        '            .footer-distributed .footer-links a{  '  +
        '               display:inline-block;  '  +
        '               line-height: 1.8;  '  +
        '               text-decoration: none;  '  +
        '               color:  inherit;  '  +
        '            }  '  +
        '     '  +
        '            .footer-distributed .footer-company-name{  '  +
        '               color:  #8f9296;  '  +
        '               font-size: 14px;  '  +
        '               font-weight: normal;  '  +
        '               margin-left: 8px;  '  +
        '            }  '  +
        '     '  +
        '            .personalName{  '  +
        '               font: normal 30px, bold;  '  +
        '               color:white;  '  +
        '            }  '  +
        '     '  +
        '            /* Footer Center */  '  +
        '     '  +
        '            .footer-distributed .footer-center{  '  +
        '               width: 35%;  '  +
        '            }  '  +
        '     '  +
        '            .footer-distributed .footer-center i{  '  +
        '               background-color:  #33383b;  '  +
        '               color: #ffffff;  '  +
        '               font-size: 25px;  '  +
        '               width: 38px;  '  +
        '               height: 38px;  '  +
        '               border-radius: 50%;  '  +
        '               text-align: center;  '  +
        '               line-height: 42px;  '  +
        '               margin: 10px 15px;  '  +
        '               vertical-align: middle;  '  +
        '            }  '  +
        '     '  +
        '            .footer-distributed .footer-center i.fa-envelope{  '  +
        '               font-size: 17px;  '  +
        '               line-height: 38px;  '  +
        '            }  '  +
        '     '  +
        '            .footer-distributed .footer-center p{  '  +
        '               display: inline-block;  '  +
        '               color: #ffffff;  '  +
        '               vertical-align: middle;  '  +
        '               margin:0;  '  +
        '            }  '  +
        '     '  +
        '            .footer-distributed .footer-center p span{  '  +
        '               display:block;  '  +
        '               font-weight: normal;  '  +
        '               font-size:14px;  '  +
        '               line-height:2;  '  +
        '            }  '  +
        '     '  +
        '            .footer-distributed .footer-center p a{  '  +
        '               color:  #5383d3;  '  +
        '               text-decoration: none;;  '  +
        '            }  '  +
        '     '  +
        '     '  +
        '            /* Footer Right */  '  +
        '     '  +
        '            .footer-distributed .footer-right{  '  +
        '               width: 20%;  '  +
        '            }  '  +
        '     '  +
        '            .footer-distributed .footer-company-about{  '  +
        '               line-height: 20px;  '  +
        '               color:  #92999f;  '  +
        '               font-size: 13px;  '  +
        '               font-weight: normal;  '  +
        '               margin: 0;  '  +
        '            }  '  +
        '     '  +
        '            .footer-distributed .footer-company-about span{  '  +
        '               display: block;  '  +
        '               color:  #ffffff;  '  +
        '               font-size: 14px;  '  +
        '               font-weight: bold;  '  +
        '               margin-bottom: 20px;  '  +
        '            }  '  +
        '     '  +
        '            .footer-distributed .footer-icons{  '  +
        '               margin-top: 25px;  '  +
        '            }  '  +
        '     '  +
        '            .footer-distributed .footer-icons a{  '  +
        '               display: inline-block;  '  +
        '               width: 35px;  '  +
        '               height: 35px;  '  +
        '               cursor: pointer;  '  +
        '               background-color:  #33383b;  '  +
        '               border-radius: 2px;  '  +
        '     '  +
        '               font-size: 20px;  '  +
        '               color: #ffffff;  '  +
        '               text-align: center;  '  +
        '               line-height: 35px;  '  +
        '     '  +
        '               margin-right: 3px;  '  +
        '               margin-bottom: 5px;  '  +
        '            }  '  +
        '     '  +
        '     '  +
        '            @media (max-width: 880px) {  '  +
        '     '  +
        '               .footer-distributed{  '  +
        '                  font: bold 14px sans-serif;  '  +
        '               }  '  +
        '     '  +
        '               .footer-distributed .footer-left,  '  +
        '               .footer-distributed .footer-center,  '  +
        '               .footer-distributed .footer-right{  '  +
        '                  display: block;  '  +
        '                  width: 100%;  '  +
        '                  margin-bottom: 40px;  '  +
        '                  text-align: center;  '  +
        '               }  '  +
        '     '  +
        '               .footer-distributed .footer-center i{  '  +
        '                  margin-left: 0;  '  +
        '               }  '  +
        '         </style>   '  +
        '      </head>  '  +
        '      <body>    '  +
        '         <md-card>  '  +
        '            <md-card-header style="background-color: black;">  '  +
        '               <md-card-header-text>  '  +
        '               Hello!  '  +
        '               </md-card-header-text>  '  +
        '            </md-card-header>  '  +
        '            <md-card-title>  '  +
        '               <md-card-title-text>  '  +
        '                  <span class="md-headline">Thank you for your interest,</span>  '  +
        '               </md-card-title-text>  '  +
        '            </md-card-title>  '  +
        '            <br>  '  +
        '            <md-card-content>  '  +
        '                <div class="row">  '  +
        '                  <div class="col-sm-12" style="margin-top: 20px">  '  +
        '                     <h4>'+name+'</h4>  '  +
        '                  </div>  '  +
        '                </div>  '  +
        "               <p>Thank you for your interest in our products. This is an automated message to confirm that your message has been received.</p>  "  +
        "               <p>We will reply to your email as soon as possible.</p>  "  +
        '            </md-card-content>  '  +
        '         </md-card>  '  +
        '         <footer class="footer-distributed">  '  +
        '            <div class="footer-left">  '  +
        "               <p class='footer-company-name' style='font-size:10px'>This email was sent to <a>"+email+"</a>, because you wanted to make contact with Fisher Supply LA, LLC. The FisherSupply web app wont send you any other message if you do not use it anymore. If you see any problem, please contact the website owner by going to the contact section on <a href='http://fishersupplyla.net'>fishersupplyla.net</a> . <br><br> Fisher Supply LA, LLC &copy; 2017</p>  "  +
        '            </div>  '  +
        '            <div class="footer-center">  '  +
        '     '  +
        '            </div>  '  +
        '     '  +
        '         </footer>  '  +
        '      </body>  '  +
        '  </html>  ' ;
    var data = {
        from: 'Fisher Supply  <gsoto@fishersupplyla.com>',
        to: email,
        subject: "Your message has been received",
        html: plantillaHtml
    };

    mailgun.messages().send(data, function(error, body){
        console.log(body);
    });
};

var sendMessage = function(name, email, message){
    var data = {
        from: 'Fisher Supply <gsoto@fishersupplyla.com>',
        to: 'gsoto@fishersupplyla.com',
        subject: 'You have a possible client!',
        text: 'Hello, this person, '+ name+' has sent you a message, ' +
        ' check it out: \n\n"' +
        message+'"\n\n You can reply to this person using this email provided: '+email+'.\n\n'+
        'Best regards from your FisherSupply website :D   \n\n\n\n'
    };

    mailgun.messages().send(data, function(error, body){
        if(error){
            console.error(error);
            res.send(error);
            return;
        }
        console.log(body);
    });
};

var sendSpecialRequest = function(data, filepath){
    var data = {
        from: 'Fisher Supply <gsoto@fishersupplyla.com>',
        to: 'gsoto@fishersupplyla.com',
        subject: 'You have a possible client!',
        text: 'Hello, this person, '+ data.name +' '+ data.lastname+' has sent you a message, ' +
        ' check it out: \n\n"' +
        data.message+'"\n\nThis is the project description:\n-'+data.project_description
        + '\n\nThis are the requirements the client is asking for: \n-'+data.requirements+
        '\n\nYou can reply to this person using this email provided: '+data.email+'.\n\n'+
        'Best regards from your FisherSupply website :D   \n\n\n\n',
        attachment: filepath
    };

    mailgun.messages().send(data, function(error, body){
        console.log(body);
    });
};

emailRouter.route('/confirmation').post(function(req, res){
    sendNotification(req.body.email, req.body.name);
    res.send("confirmation sent.");
});

emailRouter.route('/message').post(function(req, res){
    sendMessage(req.body.name, req.body.email, req.body.message);
    res.send("success");
});

emailRouter.route('/specialForm').post(function(req, res){
   var data = req.body;
   var files = './media/specialRequests';
   var fileToSend='';
   fs.readdir(files, (err, files)=>{
      files.forEach(file => {
        var sender = file.split('_');
        if(sender[0]===data.email.replace('@','-')){
            fileToSend = './media/specialRequests/'+file;
            sendSpecialRequest(data,fileToSend);
            fs.unlinkSync(fileToSend);
        }
        res.send({success:'successfully sent'});
      });
   });
});

emailRouter.route('/attachment').post(multipartyMiddleware,function (req, res) {
    var file=req.files.file;
    var user=req.body.user;
    var files = './media/specialRequests';
    let tempPath = file.path;
    let targetPath = path.join(__dirname, "../../media/specialRequests/"+
                               user.email.replace('@','-')+"_"+file.name);

    fs.readdir(files, (err, files)=>{
      files.forEach(file => {
        var sender = file.split('_');
        if(sender[0]===user.email.replace('@','-')){
            fileToDelete = './media/specialRequests/'+file;
            console.log('Will delete file: '+fileToDelete);
            fs.unlinkSync(fileToDelete);
        }
      });
    });

    fs.readFile(tempPath, function(err, data){
       if(err){
           console.error({status:500, error:err});
           res.send(err);
       }else{
           fs.writeFile(targetPath, data, function(err){
               console.log(err);
           });
           console.log("Writing new file: "+file.name);
           res.send({status:'success'});
       }
    });
});

module.exports = emailRouter;
