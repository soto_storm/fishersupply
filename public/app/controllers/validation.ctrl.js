(function(){
    angular.module('fisherApp').controller('validationCtrl',
        function($rootScope, $scope, $http, userSrv){
            $scope.checkingEmail=false;
            $scope.user={};
            $scope.message={
                validation:{}
            };
            $scope.messageSent=false;
            $scope.message.validation.is_valid=true;
            $scope.message.validation.button="darkgray";
            $scope.checkEmail=function(){
                $scope.checkingEmail=true;
                $http({
                   method:'POST',
                   url: '/api/validate/email',
                   data: $scope.user
                }).then(function successCallback(response){
                    $scope.checkingEmail=false;
                    if(!$scope.user.email){
                        $scope.message.validation.show=true;
                        $scope.message.validation.color="red";
                        $scope.message.validation.is_valid=true;
                        $scope.message.validation.text="Please fill the email field.";
                        $scope.message.validation.button="darkgray";
                        return;
                    }
                    if(response.data=='failure'){
                        $scope.message.validation.show=true;
                        $scope.message.validation.text="Invalid email";
                        $scope.message.validation.color="red";
                        $scope.message.validation.is_valid=true;
                        $scope.message.validation.button="darkgray";
                        return;
                    }else if(!response.data.is_valid){
                        $scope.message.validation.show=true;
                        $scope.message.validation.text="This is not a valid email.";
                        $scope.message.validation.color="red";
                        $scope.message.validation.is_valid=true;
                        $scope.message.validation.button="darkgray";
                        return;
                    }else if(response.statusText==='Internal Server Error'){
                        $scope.message.validation.show=true;
                        $scope.message.validation.text="Internal Server Error, please contact the " +
                                                       "site owner for more information.";
                        $scope.message.validation.color="red";
                        $scope.message.validation.is_valid=true;
                        $scope.message.validation.button="darkgray";
                    }
                    $scope.message.validation.show=true;
                    $scope.message.validation.text="This is a valid email, you might continue :)"
                    $scope.message.validation.color="green";
                    $scope.message.validation.is_valid=false;
                    $scope.message.validation.button="#008BF3";
                }, function errorCallback(err){
                    console.log(err);
                    $scope.checkEmail=false;
                });
            };

            $scope.$on('getUser', function(){
                userSrv.setUser($scope.user);
            });
            $scope.$on('messageSent', function(){
               $scope.messageSent=true;
            });
        });
}());