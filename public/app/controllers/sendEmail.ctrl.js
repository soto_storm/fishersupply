/**
 * Created by Jesus Soto on 3/11/2017.
 */
(function(){
    angular.module('fisherApp').controller('emailCtrl',
        function($rootScope, $scope, $http, userSrv){
            $scope.sendingEmail=false;
            $scope.errorMessage={};
            var user = {};
            var requirements=[];
            var fileSubmitted=false;
            // Get the modal
            var modal = document.getElementById('myModal');
            var span = document.getElementById("close");
            var modalRequest = document.getElementById('special-request');
            span.onclick = function() {
                modal.style.display = "none";
            };
            window.onclick = function(event) {
                if (event.target == modal) {
                    modal.style.display = "none";
                }
            };

            $scope.$on('fileSubmitted', function(){
                fileSubmitted=true;
            });

            $scope.addRequirement = function(requirement){                 
                 var data = requirement.split('-');
                 var expr = 'false';
                 if(requirement.match(expr)){
                     requirements.splice(data[1],1);
                 }else{
                     requirements.push(requirement);
                 }                 
                localStorage.setItem('requirements', requirements);
            };

            $scope.sendEmail=function(form){
                if(!form.$valid){
                    return;
                }

                var URL = '/api/send/message';
                if(form.$name==='specialForm'){
                    URL = '/api/send/specialForm';
                    if(!fileSubmitted){
                        $rootScope.$broadcast('fileNotSubmitted');
                        return;
                    }
                }

                $scope.$emit('getUser');
                user = userSrv.getUser();
                user.requirements = localStorage.getItem('requirements');
                user.phone=formatPhone(user.phone);
                $scope.sendingEmail=true;
                $scope.status='Sending your email...';
                $http({
                    method:'POST',
                    url: URL,
                    data: user
                }).then(function successCallback(response){
                    $http({
                      method: 'POST',
                      url:'/api/send/confirmation',
                      data:user
                    }).then(function successCallback(res){
                        $scope.sendingEmail=false;
                        $scope.errorMessage.active=false;                        
                        modal.style.display = "block";
                        modalRequest.style.display = "none";
                        $scope.$emit('messageSent');
                    })
                },function errorCallback(err){
                    console.log(err);
                    $scope.sendingEmail=false;
                    $scope.errorMessage.text="Can't connect with the server...";
                    $scope.errorMessage.active=true;
                });
            };

            function formatPhone(phone){
                var string = phone.split('');
                var formatted_phone='';
                for(var i in phone){
                    if(i==0){
                        formatted_phone+='('+phone[i];
                    }else if(i==1){
                        formatted_phone+=phone[i];
                    }else if(i==2){
                        formatted_phone+=phone[i]+') ';
                    }else if(i>2 && i<5){
                        formatted_phone+=phone[i];
                    }

                   if(i==5){                    
                        formatted_phone+=phone[i]+'-';
                    }else if(i>5){
                        formatted_phone+=phone[i];
                    }                    
                }
                return formatted_phone;
            }
        });
}());
