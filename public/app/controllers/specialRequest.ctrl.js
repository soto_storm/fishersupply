(function(){
    angular.module('fisherApp').controller('specialRequest',
        function($rootScope, $scope, userSrv, Upload){
            var modal = document.getElementById('special-request');
            var span = document.getElementsByClassName("close")[0];
            $scope.user={};
            $scope.selectedFile='Choose Files';
            $scope.submitingFile=false;
            $scope.submitMessage = {
               color:'red',
               message:'',
               activated:false
            };

            span.onclick = function() {
                modal.style.display = "none";
            };
            window.onclick = function(event) {
                if (event.target == modal) {
                    modal.style.display = "none";
                }
            };

            $scope.openForm=function(){
                modal.style.display = "block";
            };

            $scope.select = function($files){
              $scope.selectedFile=$files[0];
            };

            $scope.$on('fileNotSubmitted', function(){
                $scope.submitMessage.message = 'Please, submit a file to send a special request, otherwise use the contact form.';
                $scope.submitMessage.color = 'red';
                $scope.submitMessage.activated = true;
                $scope.submitingFile = false;
            });

            $scope.submit = function(file){
                $scope.$emit('getUser');
                $scope.user = userSrv.getUser();
                $scope.submitingFile= true;
                if(file) {
                    Upload.upload({
                        url: 'api/send/attachment',
                        method: 'POST',
                        data: {user: $scope.user},
                        file: file
                    }).progress(function (evt) {
                    }).success(function (data) {
                        console.log(data);
                        $scope.submitingFile = false;
                        $rootScope.$broadcast('fileSubmitted');
                        $scope.submitMessage.message = 'File submitted successfully';
                        $scope.submitMessage.activated = true;
                        $scope.submitMessage.color = 'green';
                    }).error(function (error) {
                        console.log('This is the error ' + error);
                        $scope.submitMessage.message = 'Error submitting the file. Check your internet.';
                        $scope.submitMessage.color = 'red';
                        $scope.submitMessage.activated = true;
                        $scope.submitingFile = false;
                    });
                }else{
                    $scope.submitMessage.message = 'Please, select a file first.';
                    $scope.submitMessage.color = 'red';
                    $scope.submitMessage.activated = true;
                    $scope.submitingFile = false;
                }
            };
        });
}());
