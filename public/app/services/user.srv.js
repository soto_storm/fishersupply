/**
 * Created by Jesus Soto on 3/1/2017.
 */
(function(){
   angular.module('fisherApp').service('userSrv', function(){
         var user = {};
         return{
            getUser: function(){
               return user;
            },
            setUser: function(data){
               user=data;
            }
         };
   });
}());